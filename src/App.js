import React, { Component } from 'react';
import './App.css';
import 'bootstrap-4-grid/css/grid.min.css';
import { studentDetails } from './studentDetails';
import { Line } from "react-chartjs-2";
import _ from 'lodash';
import MapContainer from './Map'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      studentData: studentDetails
    }

    this.dataLine = {
      labels: [],
      datasets: [
        {
          label: 'My chart',
          data: []
        }
      ]
    }
    this.uniqueStudentArray = []
    this.getCountByCollege()
  }

  componentDidMount() {
    let playername = [];
    let uniqueDateArray = [];
    let countArr = []
    let countperdate = {};

    studentDetails.forEach(element => {
      playername.push(element.Joining_Date);
    });
    playername.sort();
    uniqueDateArray = playername.filter(function (item, pos) {
      return playername.indexOf(item) === pos;
    });

    const mapByDistrict = user => user.District;
    const usersByDistrict = _.groupBy(this.state.studentData, mapByDistrict);

    console.log('usersByDistrict--', usersByDistrict)

    Object.entries(usersByDistrict).forEach(([key, value]) => {
      var countData = _.countBy(value, data => {
        return data.Joining_Date
      });
      var resultObj = {}
      console.log('countData', countData);

      Object.keys(countData).forEach(function (key, i) {
        _.some(uniqueDateArray, (item, index) => {
          if (item != key) {
            resultObj[item] = 0;
            countData = { ...resultObj, ...countData };
            console.log('key in if item!key', countData)
          } else {
          }
        })
      });

      console.log('countData--', countData)
      countArr.push(countData);
      _.forEach(countArr, (obj, i) => {
        obj = Object.values(obj)
        countperdate[key] = { obj };
      })
      console.log('countperdate---', countperdate);
    });

    const dataObj = Object.values(countperdate).map(userArr => userArr);

    this.setState({
      Data: {
        labels: uniqueDateArray,
        datasets: [
          {
            label: 'Kurnool',
            data: dataObj[0].obj,
            fill: true,
            lineTension: 0.3,
            backgroundColor: "pink",
            borderColor: "rgb(205, 130, 158)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "hotpink",
            pointBackgroundColor: "pink",
            pointBorderWidth: 10,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgb(0, 0, 0)",
            pointHoverBorderColor: "rgba(220, 220, 220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
          },
          {
            label: 'Ananthapur',
            data: dataObj[1].obj,
            fill: true,
            lineTension: 0.3,
            backgroundColor: "bisque",
            borderColor: "rgb(205, 130, 158)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "bisque",
            pointBackgroundColor: "bisque",
            pointBorderWidth: 10,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgb(0, 0, 0)",
            pointHoverBorderColor: "rgba(220, 220, 220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
          }, {
            label: 'Chittor',
            data: dataObj[2].obj,
            fill: true,
            lineTension: 0.3,
            backgroundColor: "aquamarine",
            borderColor: "rgb(35, 26, 136)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "aquamarine",
            pointBackgroundColor: "aquamarine",
            pointBorderWidth: 10,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgb(0, 0, 0)",
            pointHoverBorderColor: "rgba(220, 220, 220, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
          }, {
            label: 'East Godavari',
            data: dataObj[3].obj,
            fill: true,
            lineTension: 0.3,
            backgroundColor: "cadetblue",
            borderColor: "rgb(35, 26, 136)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "cadetblue",
            pointBackgroundColor: "cadetblue",
            pointBorderWidth: 10,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgb(0, 0, 0)",
            pointHoverBorderColor: "rgba(220, 220, 220, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
          }, {
            label: 'West Godavari',
            data: dataObj[4].obj,
            fill: true,
            lineTension: 0.3,
            backgroundColor: "darkgrey",
            borderColor: "rgb(35, 26, 136)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "darkgrey",
            pointBackgroundColor: "darkgrey",
            pointBorderWidth: 10,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgb(0, 0, 0)",
            pointHoverBorderColor: "rgba(220, 220, 220, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
          },
          {
            label: 'Vishakhapatnam',
            data: dataObj[5].obj,
            fill: true,
            lineTension: 0.3,
            backgroundColor: "darkturquoise",
            borderColor: "rgb(35, 26, 136)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "darkturquoise",
            pointBackgroundColor: "darkturquoise",
            pointBorderWidth: 10,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgb(0, 0, 0)",
            pointHoverBorderColor: "rgba(220, 220, 220, 1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
          }
        ]
      }
    });
  }

  getCountByCollege() {
    var studentInfo = [];
    const countByCollege = _.groupBy(this.state.studentData, (user) => user.College
    );
    Object.values(countByCollege).map(arr => {
      arr.forEach(data => {
        studentInfo.push({
          City: data.City, District: data.District,
          College: data.College, Count: arr.length
        })
      })
    })

    const map = new Map();
    for (const item of studentInfo) {
      if (!map.has(item.College)) {
        map.set(item.College, true);
        this.uniqueStudentArray.push({
          City: item.City, District: item.District,
          College: item.College, Count: item.Count
        });
      }
    }
  }

  render() {
    return (
      <div className="bootstrap-wrapper">
        <div className="app-container container">
          <div className="row">
            <div className="col-md-12 azureColor">
              <h1>Dashboard</h1>
            </div>
          </div>
          <div className="row rowHeight">
            <div className="col-lg-12">
              <div className="row blueColor text-center">
                <div className="text-white text-props">TIMELINE</div>
              </div>
              <div className="row">
                <Line data={this.state.Data} width={350} height={304} options={{ maintainAspectRatio: false }} />
              </div>
            </div>
          </div>
        </div>

        <div className="app-container container">
          <div className="row">
            <div className="col-lg-12">
              <div className="row blueColor text-center">
                <div className="text-white text-props">REPORT</div>
              </div>
              <div className="row">
                <table id="t01">
                  <thead>
                    <tr>
                      <th>City</th>
                      <th>District</th>
                      <th>College</th>
                      <th>Count</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      this.uniqueStudentArray.map(data => {
                        return (
                          <tr>
                            <td>{data.City}</td>
                            <td>{data.District}</td>
                            <td>{data.College}</td>
                            <td>{data.Count}</td>
                          </tr>
                        )
                      })
                    }
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        


        <div className="app-container container">
         
          <div className="row rowHeight">
            <div className="col-lg-12">
              <div className="row blueColor text-center">
                <div className="text-white text-props">MAP VIEW</div>
              </div>
              <div className="row">
              <MapContainer />
              </div>
            </div>
          </div>
        </div>

      </div >
    );
  }
}


export default App;