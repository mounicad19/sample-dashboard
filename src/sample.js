state = {
    rawData = null; // by default
  }
//   componentDidMount() {
//     // your code to fetch data via REST API, GraphQL, etc.
//     .then(response => {
//       // If response has data
//       if (response.data) {
//         // Add response data to state to force re-render of components
//         this.setState({
//           rawData: response.data,
//         });
//       }
//     });
//   }
  prepareChartData() {
    const { rawData } = this.state.rawData;
    // If we have received the data we need to render chart
    if (rawData !== null) {
      // group users by device type
      const mapByDevice = user => user.deviceType;
      const usersPerDevice = groupBy(rawData, mapByDevice); // for ex: { { iPhone XS: [ { name: John, ... }, { name: Jade, ... }, ... ] }
      
      // Prepare data for chartJS    
      const data = Object.values(appsByDemo).map(users => users.length); // for ex: [ 22, 44, 35, ...]
      const labels = Object.keys(usersPerDevice); // for ex: [ iPhone XS, Pixel 3, ... ]
      return { labels, data };
  }
  renderBarChart() {
    const { labels, data } = prepareChartData();
    return (
      <Bar
        data={
          labels: labels,
          datasets: [{
            label: "# Users per device",
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: data,
          }]
        }
      />
    );
  }
  render() {
    {this.renderBarChart}
  }