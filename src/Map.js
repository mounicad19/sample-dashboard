import React, { Component } from 'react';
import { Map, Marker, GoogleApiWrapper} from 'google-maps-react';

const mapStyles = {
    position: 'inherit',
    width: '100%',
    height: '100%',
};

class MapContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            stores: [{ lat: 14.685564, lng: 77.595406 },
            { latitude: 13.217096, longitude: 79.100677 },
            { latitude: 17.8333, longitude: 81.8333 },
            { latitude: 15.834536	, longitude: 78.029366 },
            { latitude: 17.6868339, longitude: 83.2184635 },
            { latitude: 16.1495107, longitude: 79.8618773 }]
        }
    }

    displayMarkers = () => {
        return this.state.stores.map((store, index) => {
            return <Marker key={index} id={index} position={{
                lat: store.latitude,
                lng: store.longitude
            }}
                onClick={() => console.log("You clicked me!")} />
        })
    }

    render() {
        return (
            <div>
                <Map
                    google={this.props.google}
                    zoom={8}
                    style={mapStyles}
                    initialCenter={{ lat: 47.444, lng: -122.176 }}
                >
                    {this.displayMarkers()}
                </Map>
            </div>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: ('AIzaSyCL6EP2bkZUXE_NQNxm09JcAwgIgv6zTqE')
  })(MapContainer)